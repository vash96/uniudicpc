# UniudICPC
Soluzioni di problemi risolti durante gli allenamenti per le ACM-ICPC

# Organizzazione
Cercate di rispettare la gerarchia definita da `sito/problema/risolutore_complessità`.  
Per `codeforces` il nome è definito da `contest/lettera`.  
La cartella `swerc` contiene i problemi risolti delle edizioni delle ICPC della nostra regione.  
All'inizio del sorgente inserite il link di riferimento al testo del problema e spiegate brevemente le idee risolutive e la complessità dell'algoritmo. Se in rete ci sono spunti utili sulle tecniche usate linkatele.